package services

import (
	"encoding/json"
	"math/rand"
	"os"

	"gitlab.com/VFay/pubg-lander/utils"
)

type LocationService struct {
	locations map[string][]string
}

func (s *LocationService) Init() {
	file, _ := os.ReadFile(utils.GetDataFilePath())

	locations := map[string][]string{}
	_ = json.Unmarshal([]byte(file), &locations)

	s.locations = locations
}

func (s *LocationService) GetRandomLocation(key string) string {
	mapLocations := s.locations[key]

	if mapLocations == nil {
		return "Invalid map key"
	}

	locationIndex := rand.Intn(len(mapLocations))
	return mapLocations[locationIndex]
}
