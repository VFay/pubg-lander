package utils

const (
	BotPrefix string = "!plander"
	TokenKey  string = "TOKEN"
)

var AvailableMaps = []string{"erangel", "miramar", "sanhok"}

func GetDataFilePath() string {
	return "assets/data.json"
}
