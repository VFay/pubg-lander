package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"slices"
	"strings"
	"syscall"

	"github.com/bwmarrin/discordgo"
	"github.com/joho/godotenv"

	"gitlab.com/VFay/pubg-lander/services"
	"gitlab.com/VFay/pubg-lander/utils"
)

func main() {
	if err := godotenv.Load(); err != nil {
		log.Fatal("Error loading .env file")
	}

	var service services.LocationService
	service.Init()

	token := os.Getenv(utils.TokenKey)
	session, err := discordgo.New("Bot " + token)

	if err != nil {
		log.Fatal(err)
	}

	session.AddHandler(func(s *discordgo.Session, m *discordgo.MessageCreate) {
		if m.Author.ID == s.State.User.ID {
			return
		}

		args := strings.Split(m.Content, " ")

		if len(args) == 0 || args[0] != utils.BotPrefix {
			return
		}

		if slices.Contains(utils.AvailableMaps, args[1]) {
			randomLocation := service.GetRandomLocation(args[1])

			s.ChannelMessageSend(m.ChannelID, randomLocation)
		}
	})

	session.Identify.Intents = discordgo.IntentsAllWithoutPrivileged

	err = session.Open()

	if err != nil {
		log.Fatal(err)
	}
	defer session.Close()

	fmt.Println("bot online")

	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt)
	<-sc
}
